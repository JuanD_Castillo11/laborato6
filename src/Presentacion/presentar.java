package Presentacion;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import logica.Curso;
public class presentar {
 
	private static final String FILE_NAME = "cursos.ser";
	private static List<Curso> cursos = new ArrayList<>();
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws FileNotFoundException, ClassNotFoundException, IOException {
        cargarCursos();
        boolean salir = false;

        while (!salir) {
            System.out.println("\n** Menú **");
            System.out.println("1. Agregar Curso");
            System.out.println("2. Consultar Cursos");
            System.out.println("3. Actualizar un Curso");
            System.out.println("4. Almacenar Cursos en Archivo");
            System.out.println("5. Cargar Cursos de Archivo");
            System.out.println("6. Salir");
            System.out.print("Ingrese su opción: ");
            int opcion = scanner.nextInt();
            scanner.nextLine(); 

            switch (opcion) {
                case 1:
                    agregarCurso();
                    break;
                case 2:
                    consultarCursos();
                    break;
                case 3:
                    actualizarCurso();
                    break;
                case 4:
                    almacenarCursos();
                    break;
                case 5:
                    cargarCursos();
                    break;
                case 6:
                    salir = true;
                    break;
                default:
                    System.out.println("Opción no válida. Por favor, ingrese una opción válida.");
            }
        }
    }
    
    
    private static void agregarCurso() {
        System.out.print("Ingrese el ID del curso: ");
        int id = scanner.nextInt();
        scanner.nextLine(); 
        System.out.print("Ingrese el nombre del curso: ");
        String nombre = scanner.nextLine();
        System.out.print("Ingrese los créditos del curso: ");
        int creditos = scanner.nextInt();
        scanner.nextLine(); 

        Curso curso = new Curso(id, nombre, creditos);
        cursos.add(curso);
        System.out.println("Curso agregado correctamente.");
    }

    private static void consultarCursos() {
        if (cursos.isEmpty()) {
            System.out.println("No hay cursos disponibles.");
        } else {
            System.out.println("Lista de cursos:");
            for (Curso curso : cursos) {
                System.out.println(curso);
            }
        }
    }

    private static void actualizarCurso() {
        System.out.print("Ingrese el ID del curso que desea actualizar: ");
        int id = scanner.nextInt();
        scanner.nextLine(); 

        boolean encontrado = false;
        for (Curso curso : cursos) {
            if (curso.getId() == id) {
                System.out.print("Ingrese el nuevo nombre del curso: ");
                String nombre = scanner.nextLine();
                System.out.print("Ingrese los nuevos créditos del curso: ");
                int creditos = scanner.nextInt();
                scanner.nextLine();

                curso.setNombre(nombre);
                curso.setCreditos(creditos);
                encontrado = true;
                System.out.println("Curso actualizado correctamente.");
                break;
            }
        }
        if (!encontrado) {
            System.out.println("Curso no encontrado.");
        }
    }

 public static void almacenarCursos() {
        try (ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(FILE_NAME))) {
            outputStream.writeObject(cursos);
            System.out.println("Cursos almacenados en archivo correctamente.");
        } catch (IOException e) {
            System.err.println("Error al almacenar los cursos en archivo: " + e.getMessage());
        }
    }

    @SuppressWarnings("unchecked")
    public static void cargarCursos() {
        try (ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(FILE_NAME))) {
            cursos = (List<Curso>) inputStream.readObject();
            System.out.println("Cursos cargados desde archivo correctamente.");
        } catch (IOException | ClassNotFoundException e) {
            System.err.println("Error al cargar los cursos desde archivo: " + e.getMessage());
        }
    }
}
